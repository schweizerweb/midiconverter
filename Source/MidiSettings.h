/*
  ==============================================================================

    MidiSettings.h
    Created: 1 Feb 2018 8:54:13am
    Author:  Christian Schweizer

  ==============================================================================
*/

#pragma once
#include "JuceHeader.h"

class MidiSettings
{
public:
	MidiSettings();

	ScopedPointer<MidiFile> file;
	int noteLengthMs;
	int minIntervalMs;
	String outputFilePath;
	enum eAlgorithm { None, Simple, Ablinger3a, Ablinger3b } algorithm;
};