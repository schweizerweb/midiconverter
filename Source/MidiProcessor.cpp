/*
  ==============================================================================

    MidiProcessor.cpp
    Created: 25 Feb 2018 11:56:03pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#include "MidiProcessor.h"

MidiProcessor::~MidiProcessor()
{
}

void MidiProcessor::prepareNoteArrays(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double* tickLengthMs, bool generatePseudoHighResolution)
{
	short timeFormat = settings->file->getTimeFormat();

	for (int i = 0; i < input->getNumEvents(); i++)
	{
		MidiMessageSequence::MidiEventHolder* ev = input->getEventPointer(i);
		if (ev->message.isTempoMetaEvent())
		{
			*tickLengthMs = ev->message.getTempoMetaEventTickLength(timeFormat) * 1000.0;
		}

		// direct copy all meta events
		if (!ev->message.isNoteOnOrOff())
		{
			output->addEvent(ev->message);
			continue;
		}

		// save ON events to note specific arrays
		if (ev->message.isNoteOn())
		{
			int channelNumber = ev->message.getChannel();
			// ignore invalid channels
			if (channelNumber >= 1 && channelNumber <= 16)
			{
				int noteNumber = ev->message.getNoteNumber();
				uint8 velocity = ev->message.getVelocity();
				double startTimestamp = ev->message.getTimeStamp();

				if (generatePseudoHighResolution)
				{
					double duration = ev->noteOffObject->message.getTimeStamp() - ev->message.getTimeStamp();
					for (int iTime = 0; iTime < duration; iTime++)
					{
						MidiMessage msg = MidiMessage::noteOn(channelNumber, noteNumber, velocity);
						msg.setTimeStamp(startTimestamp + iTime);
						noteSortedEvents[noteNumber].add(new MidiMessage(msg));
					}
				}
				else
					noteSortedEvents[noteNumber].add(new MidiMessage(ev->message));
			}
		}

		// (ignore note OFF events)
	}

	// convert ms to ticks
	minNoteLength = getTicks(settings->noteLengthMs, timeFormat, *tickLengthMs);
	minInterval = getTicks(settings->minIntervalMs, timeFormat, *tickLengthMs);
	minBreakLength = minInterval - minNoteLength;
}

double MidiProcessor::getTicks(int ms, short timeFormat, double tickLengthMs)
{
	if (timeFormat > 0)
	{
		//return (60000 / (timeFormat)) * secondsPerQuarterNote;
		return ms / tickLengthMs;
	}
	else
	{
		return ms * 0.001;
	}
}

void MidiProcessor::completeNote(MidiMessageSequence* pTrack, Array<float>* velocities, MidiMessage* startMessage, double noteLength) const
{
	velocities->sort();
	startMessage->setVelocity(velocities->getLast());
	pTrack->addEvent(*startMessage);

	MidiMessage stopMessage = MidiMessage::noteOff(startMessage->getChannel(), startMessage->getNoteNumber());
	stopMessage.setTimeStamp(startMessage->getTimeStamp() + noteLength);
	pTrack->addEvent(stopMessage);

	// reset
	velocities->clear();
}
