/*
  ==============================================================================

    MidiMessageComparator.h
    Created: 27 Feb 2018 9:55:09am
    Author:  Christian Schweizer

  ==============================================================================
*/

#pragma once
#include "JuceHeader.h"

class MidiMessageComparator
{
public:
	static int compareElements(MidiMessage a, MidiMessage b);
};