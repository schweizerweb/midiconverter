/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "MidiProcessor.h"
#include "MidiProcessorFactory.h"

//==============================================================================
MainContentComponent::MainContentComponent()
{
	// properties handling
	PropertiesFile::Options options;
	options.applicationName = ProjectInfo::projectName;
	options.storageFormat = PropertiesFile::storeAsXML;
	properties.setStorageParameters(options);
	userSettings = properties.getUserSettings();

	// load settings
	settings = new MidiSettings();
	settings->noteLengthMs = userSettings->getIntValue("noteLengthMs", 72);
	settings->minIntervalMs = userSettings->getIntValue("minIntervalMs", 96);
	settings->algorithm = MidiSettings::eAlgorithm(userSettings->getIntValue("algorithm", MidiSettings::Simple));

    addAndMakeVisible(settingsComponent = new SettingsComponent(settings));
    addAndMakeVisible(startButton = new TextButton("Start"));
	startButton->addListener(this);
    
    setSize (600, 400);
}

MainContentComponent::~MainContentComponent()
{
	userSettings->setValue("noteLengthMs", settings->noteLengthMs);
	userSettings->setValue("minIntervalMs", settings->minIntervalMs);
	userSettings->setValue("algorithm", settings->algorithm);
}

void MainContentComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setFont (Font (16.0f));
    g.setColour (Colours::white);
    g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainContentComponent::resized()
{
	Rectangle<int> b = getBounds();
	settingsComponent->setBounds(b.withBottom(b.getHeight()-30));
	startButton->setBounds(b.withHeight(30).withBottomY(b.getBottom()));
}

bool MainContentComponent::checkSettingsValid() const
{
	if (settings->file == nullptr)
	{
		AlertWindow::showMessageBox(AlertWindow::AlertIconType::WarningIcon, ProjectInfo::projectName, "No input file loaded!");
		return false;
	}

	if (settings->minIntervalMs < settings->noteLengthMs)
	{
		AlertWindow::showMessageBox(AlertWindow::AlertIconType::WarningIcon, ProjectInfo::projectName, "Required note length is longer than the minimal interval!");
		return false;
	}

	return true;
}

bool MainContentComponent::startProcess() const
{
	if (!checkSettingsValid()) 
		return false;

	MidiProcessor* midiProcessor = MidiProcessorFactory::getProcessor(settings->algorithm);
	if (midiProcessor == nullptr)
	{
		AlertWindow::showMessageBox(AlertWindow::AlertIconType::WarningIcon, ProjectInfo::projectName, "Unable to create processor for algorithm type");
		return false;
	}

	MouseCursor::showWaitCursor();
	ScopedPointer<MidiFile> newFile = new MidiFile(*settings->file);
	OwnedArray<MidiMessageSequence> newTracks;
	int numTracks = newFile->getNumTracks();
	double tickLengthMs = 1.0; // default

	for (int iTrack = 0; iTrack < numTracks; iTrack++)
	{
		MidiMessageSequence* newTrack = new MidiMessageSequence();
		const MidiMessageSequence* inputSequence = newFile->getTrack(iTrack);

		midiProcessor->process(inputSequence, newTrack, settings, &tickLengthMs);

		String metaEventString = "(c) MidiConverter " + String(ProjectInfo::versionString) + String("; Algorithm ") + String(settings->algorithm);
		newTrack->addEvent(MidiMessage::textMetaEvent(0x01, metaEventString));

		// clean up
		newTrack->sort();
		newTrack->updateMatchedPairs();
		newTracks.add(newTrack);
	}

	// output
	newFile->clear();
	for (MidiMessageSequence* track : newTracks)
	{	
		newFile->addTrack(*track);
	}
	
	String outputFilename = settings->outputFilePath;
	ScopedPointer<File> outFile = new File(outputFilename);
	if(outFile->existsAsFile())
	{
		if (!AlertWindow::showOkCancelBox(AlertWindow::AlertIconType::WarningIcon, ProjectInfo::projectName, "Target file exists, overwrite?"))
		{
			MouseCursor::hideWaitCursor();
			return false;
		}
	}

	outFile->create();
	outFile->deleteFile();
	ScopedPointer<OutputStream> outStream = outFile->createOutputStream();
	newFile->writeTo(*outStream);

	delete midiProcessor;

	MouseCursor::hideWaitCursor();

	return true;
}

void MainContentComponent::buttonClicked(Button* btn)
{
	if(btn == startButton)
	{
		if (startProcess())
		{
			AlertWindow::showMessageBox(AlertWindow::InfoIcon, ProjectInfo::projectName, "Process completed");
		}
	}
}
