/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.3.0

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "SettingsComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
SettingsComponent::SettingsComponent (MidiSettings* pSettings)
    : pSettings(pSettings)
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    addAndMakeVisible (textMidiFile = new TextEditor ("textMidiFile"));
    textMidiFile->setMultiLine (false);
    textMidiFile->setReturnKeyStartsNewLine (false);
    textMidiFile->setReadOnly (true);
    textMidiFile->setScrollbarsShown (false);
    textMidiFile->setCaretVisible (false);
    textMidiFile->setPopupMenuEnabled (true);
    textMidiFile->setText (String());

    addAndMakeVisible (labelMidiFile = new Label ("midiFile",
                                                  TRANS("MIDI file")));
    labelMidiFile->setFont (Font (15.00f, Font::plain).withTypefaceStyle ("Regular"));
    labelMidiFile->setJustificationType (Justification::centredLeft);
    labelMidiFile->setEditable (false, false, false);
    labelMidiFile->setColour (TextEditor::textColourId, Colours::black);
    labelMidiFile->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    labelMidiFile->setBounds (8, 8, 136, 24);

    addAndMakeVisible (btnChooseFile = new TextButton ("btnChooseFile"));
    btnChooseFile->setButtonText (TRANS("..."));
    btnChooseFile->addListener (this);

    addAndMakeVisible (labelMinInterval = new Label ("minInterval",
                                                     TRANS("Min Interval [ms]")));
    labelMinInterval->setFont (Font (15.00f, Font::plain).withTypefaceStyle ("Regular"));
    labelMinInterval->setJustificationType (Justification::centredLeft);
    labelMinInterval->setEditable (false, false, false);
    labelMinInterval->setColour (TextEditor::textColourId, Colours::black);
    labelMinInterval->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    labelMinInterval->setBounds (8, 72, 136, 24);

    addAndMakeVisible (sliderMinInterval = new Slider ("sliderMinInterval"));
    sliderMinInterval->setRange (1, 1000, 1);
    sliderMinInterval->setSliderStyle (Slider::IncDecButtons);
    sliderMinInterval->setTextBoxStyle (Slider::TextBoxLeft, false, 80, 20);
    sliderMinInterval->addListener (this);

    sliderMinInterval->setBounds (152, 72, 160, 24);

    addAndMakeVisible (labelToneLength = new Label ("toneLength",
                                                    TRANS("Tone length [ms]")));
    labelToneLength->setFont (Font (15.00f, Font::plain).withTypefaceStyle ("Regular"));
    labelToneLength->setJustificationType (Justification::centredLeft);
    labelToneLength->setEditable (false, false, false);
    labelToneLength->setColour (TextEditor::textColourId, Colours::black);
    labelToneLength->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    labelToneLength->setBounds (9, 104, 136, 24);

    addAndMakeVisible (sliderToneLength = new Slider ("sliderToneLength"));
    sliderToneLength->setRange (1, 1000, 1);
    sliderToneLength->setSliderStyle (Slider::IncDecButtons);
    sliderToneLength->setTextBoxStyle (Slider::TextBoxLeft, false, 80, 20);
    sliderToneLength->addListener (this);

    sliderToneLength->setBounds (153, 104, 159, 24);

    addAndMakeVisible (textOutputName = new TextEditor ("textOutputName"));
    textOutputName->setMultiLine (false);
    textOutputName->setReturnKeyStartsNewLine (false);
    textOutputName->setReadOnly (false);
    textOutputName->setScrollbarsShown (false);
    textOutputName->setCaretVisible (true);
    textOutputName->setPopupMenuEnabled (true);
    textOutputName->setText (String());

    addAndMakeVisible (labelOutput = new Label ("midiOutput",
                                                TRANS("Output")));
    labelOutput->setFont (Font (15.00f, Font::plain).withTypefaceStyle ("Regular"));
    labelOutput->setJustificationType (Justification::centredLeft);
    labelOutput->setEditable (false, false, false);
    labelOutput->setColour (TextEditor::textColourId, Colours::black);
    labelOutput->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    labelOutput->setBounds (8, 40, 136, 24);

    addAndMakeVisible (labelAlgorithm = new Label ("algorithm",
                                                   TRANS("Algorithm")));
    labelAlgorithm->setFont (Font (15.00f, Font::plain).withTypefaceStyle ("Regular"));
    labelAlgorithm->setJustificationType (Justification::centredLeft);
    labelAlgorithm->setEditable (false, false, false);
    labelAlgorithm->setColour (TextEditor::textColourId, Colours::black);
    labelAlgorithm->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    labelAlgorithm->setBounds (8, 136, 136, 24);

    addAndMakeVisible (comboBoxAlgorithm = new ComboBox ("comboBoxAlgorithm"));
    comboBoxAlgorithm->setEditableText (false);
    comboBoxAlgorithm->setJustificationType (Justification::centredLeft);
    comboBoxAlgorithm->setTextWhenNothingSelected (String());
    comboBoxAlgorithm->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    comboBoxAlgorithm->addListener (this);

    comboBoxAlgorithm->setBounds (152, 136, 160, 24);


    //[UserPreSize]

	comboBoxAlgorithm->addItem("Simple", MidiSettings::Simple);
	comboBoxAlgorithm->addItem("Ablinger 3a", MidiSettings::Ablinger3a);
	comboBoxAlgorithm->addItem("Ablinger 3b", MidiSettings::Ablinger3b);
	comboBoxAlgorithm->setSelectedId(pSettings->algorithm);

    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
	sliderMinInterval->setRange(1.0, DBL_MAX, 1.0);
	sliderToneLength->setRange(1.0, DBL_MAX, 1.0);
	sliderMinInterval->setValue(pSettings->minIntervalMs);
	sliderToneLength->setValue(pSettings->noteLengthMs);
	textOutputName->addListener(this);
    //[/Constructor]
}

SettingsComponent::~SettingsComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    textMidiFile = nullptr;
    labelMidiFile = nullptr;
    btnChooseFile = nullptr;
    labelMinInterval = nullptr;
    sliderMinInterval = nullptr;
    labelToneLength = nullptr;
    sliderToneLength = nullptr;
    textOutputName = nullptr;
    labelOutput = nullptr;
    labelAlgorithm = nullptr;
    comboBoxAlgorithm = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void SettingsComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff505050));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void SettingsComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    textMidiFile->setBounds (152, 8, getWidth() - 219, 24);
    btnChooseFile->setBounds (getWidth() - 51, 8, 39, 24);
    textOutputName->setBounds (152, 40, getWidth() - 219, 24);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void SettingsComponent::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == btnChooseFile)
    {
        //[UserButtonCode_btnChooseFile] -- add your button handler code here..
		FileChooser dlg("MIDI-File", File::getSpecialLocation(File::userDesktopDirectory), "*.mid");
		if (dlg.browseForFileToOpen())
		{
			File inputFile = dlg.getResult();
			ScopedPointer<FileInputStream> inStream = inputFile.createInputStream();
			ScopedPointer<MidiFile> midiFile = new MidiFile();
			bool result = midiFile->readFrom(*inStream);

			if (result)
			{
				textMidiFile->setText(dlg.getResult().getFileName());
				pSettings->outputFilePath = dlg.getResult().getFullPathName().dropLastCharacters(4) + "_corr.mid";
				pSettings->file = midiFile;
				textOutputName->setText(pSettings->outputFilePath);
			}
			else
			{
				textMidiFile->setText("Error reading file");
				pSettings->outputFilePath.clear();
				pSettings->file = nullptr;
				textOutputName->clear();
			}
		}
        //[/UserButtonCode_btnChooseFile]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}

void SettingsComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == sliderMinInterval)
    {
        //[UserSliderCode_sliderMinInterval] -- add your slider handling code here..
		pSettings->minIntervalMs = int(sliderMinInterval->getValue());
        //[/UserSliderCode_sliderMinInterval]
    }
    else if (sliderThatWasMoved == sliderToneLength)
    {
        //[UserSliderCode_sliderToneLength] -- add your slider handling code here..
		pSettings->noteLengthMs = int(sliderToneLength->getValue());
        //[/UserSliderCode_sliderToneLength]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}

void SettingsComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    //[UsercomboBoxChanged_Pre]
    //[/UsercomboBoxChanged_Pre]

    if (comboBoxThatHasChanged == comboBoxAlgorithm)
    {
        //[UserComboBoxCode_comboBoxAlgorithm] -- add your combo box handling code here..
		pSettings->algorithm = MidiSettings::eAlgorithm(comboBoxAlgorithm->getSelectedId());
        //[/UserComboBoxCode_comboBoxAlgorithm]
    }

    //[UsercomboBoxChanged_Post]
    //[/UsercomboBoxChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void SettingsComponent::textEditorTextChanged(TextEditor& textEditor)
{
	if(&textEditor == textOutputName)
	{
		pSettings->outputFilePath = textOutputName->getText();
	}
}

//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="SettingsComponent" componentName=""
                 parentClasses="public Component, public TextEditor::Listener"
                 constructorParams="MidiSettings* pSettings" variableInitialisers="pSettings(pSettings)"
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff505050"/>
  <TEXTEDITOR name="textMidiFile" id="2c9d62fb3e002bb" memberName="textMidiFile"
              virtualName="" explicitFocusOrder="0" pos="152 8 219M 24" initialText=""
              multiline="0" retKeyStartsLine="0" readonly="1" scrollbars="0"
              caret="0" popupmenu="1"/>
  <LABEL name="midiFile" id="6de54f8cca0da185" memberName="labelMidiFile"
         virtualName="" explicitFocusOrder="0" pos="8 8 136 24" edTextCol="ff000000"
         edBkgCol="0" labelText="MIDI file" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15.00000000000000000000"
         kerning="0.00000000000000000000" bold="0" italic="0" justification="33"/>
  <TEXTBUTTON name="btnChooseFile" id="8763407a63d0aaa3" memberName="btnChooseFile"
              virtualName="" explicitFocusOrder="0" pos="51R 8 39 24" buttonText="..."
              connectedEdges="0" needsCallback="1" radioGroupId="0"/>
  <LABEL name="minInterval" id="f8a12b80000a227b" memberName="labelMinInterval"
         virtualName="" explicitFocusOrder="0" pos="8 72 136 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Min Interval [ms]" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15.00000000000000000000" kerning="0.00000000000000000000"
         bold="0" italic="0" justification="33"/>
  <SLIDER name="sliderMinInterval" id="6e207db7d8ba90fe" memberName="sliderMinInterval"
          virtualName="" explicitFocusOrder="0" pos="152 72 160 24" min="1.00000000000000000000"
          max="1000.00000000000000000000" int="1.00000000000000000000"
          style="IncDecButtons" textBoxPos="TextBoxLeft" textBoxEditable="1"
          textBoxWidth="80" textBoxHeight="20" skewFactor="1.00000000000000000000"
          needsCallback="1"/>
  <LABEL name="toneLength" id="6c64668fbc28b521" memberName="labelToneLength"
         virtualName="" explicitFocusOrder="0" pos="9 104 136 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Tone length [ms]" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15.00000000000000000000" kerning="0.00000000000000000000"
         bold="0" italic="0" justification="33"/>
  <SLIDER name="sliderToneLength" id="b409e5b14df40fcd" memberName="sliderToneLength"
          virtualName="" explicitFocusOrder="0" pos="153 104 159 24" min="1.00000000000000000000"
          max="1000.00000000000000000000" int="1.00000000000000000000"
          style="IncDecButtons" textBoxPos="TextBoxLeft" textBoxEditable="1"
          textBoxWidth="80" textBoxHeight="20" skewFactor="1.00000000000000000000"
          needsCallback="1"/>
  <TEXTEDITOR name="textOutputName" id="349a6b45f8db59c3" memberName="textOutputName"
              virtualName="" explicitFocusOrder="0" pos="152 40 219M 24" initialText=""
              multiline="0" retKeyStartsLine="0" readonly="0" scrollbars="0"
              caret="1" popupmenu="1"/>
  <LABEL name="midiOutput" id="78ff71d4a59e1812" memberName="labelOutput"
         virtualName="" explicitFocusOrder="0" pos="8 40 136 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Output" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15.00000000000000000000"
         kerning="0.00000000000000000000" bold="0" italic="0" justification="33"/>
  <LABEL name="algorithm" id="e20d579778f5fe3d" memberName="labelAlgorithm"
         virtualName="" explicitFocusOrder="0" pos="8 136 136 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Algorithm" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15.00000000000000000000"
         kerning="0.00000000000000000000" bold="0" italic="0" justification="33"/>
  <COMBOBOX name="comboBoxAlgorithm" id="8ef9dfcff56f7733" memberName="comboBoxAlgorithm"
            virtualName="" explicitFocusOrder="0" pos="152 136 160 24" editable="0"
            layout="33" items="" textWhenNonSelected="" textWhenNoItems="(no choices)"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
