/*
  ==============================================================================

    MidiProcessorFactory.cpp
    Created: 25 Feb 2018 11:26:25pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#include "MidiProcessorFactory.h"
#include "MidiProcessorSimple.h"
#include "MidiProcessorAblingerNew.h"

MidiProcessor* MidiProcessorFactory::getProcessor(MidiSettings::eAlgorithm algorithm)
{
	switch (algorithm)
	{
	case MidiSettings::Simple: return new MidiProcessorSimple();
	case MidiSettings::Ablinger3a: return new MidiProcessorAblingerNew(false);
	case MidiSettings::Ablinger3b: return new MidiProcessorAblingerNew(true);
	default: return nullptr;
	}
}
