/*
  ==============================================================================

    MidiMessageComparator.cpp
    Created: 27 Feb 2018 9:55:09am
    Author:  Christian Schweizer

  ==============================================================================
*/

#include "MidiMessageComparator.h"

int MidiMessageComparator::compareElements(MidiMessage a, MidiMessage b)
{
	if (a.getTimeStamp() < b.getTimeStamp())
		return -1;
	else if (a.getTimeStamp() > b.getTimeStamp())
		return 1;
	else // if a == b
		return 0;
}
