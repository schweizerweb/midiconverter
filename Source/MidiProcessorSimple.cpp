/*
  ==============================================================================

    MidiProcessorSimple.cpp
    Created: 25 Feb 2018 11:25:30pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#include "MidiProcessorSimple.h"

MidiProcessorSimple::~MidiProcessorSimple()
{
}

void MidiProcessorSimple::process(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double *tickLengthMs)
{
	prepareNoteArrays(input, output, settings, tickLengthMs, true);

	for (int iNote = 0; iNote < 128; iNote++)
	{
		if (noteSortedEvents[iNote].size() < 1)
			continue;

		MidiMessage* startMessage = noteSortedEvents[iNote].getUnchecked(0);
		double lastEndTimestamp = startMessage->getTimeStamp() + 1.0;

		Array<float> velocities;
		for (int i = 0; i < noteSortedEvents[iNote].size(); i++)
		{
			MidiMessage* currentEvent = noteSortedEvents[iNote].getUnchecked(i);
			double currentTimestamp = currentEvent->getTimeStamp();
			double firstTimestamp = startMessage->getTimeStamp();
			if (currentTimestamp >= firstTimestamp + minInterval)
			{
				// complete pending note
				double should = minInterval;
				double maximum = currentTimestamp - firstTimestamp - minBreakLength;
				double noteLength = jmax(minNoteLength, jmin(should, maximum));
				completeNote(output, &velocities, startMessage, noteLength);
				startMessage = currentEvent;
			}

			velocities.add(currentEvent->getFloatVelocity());
			lastEndTimestamp = currentEvent->getTimeStamp() + 1.0;
		}

		if (velocities.size() > 0)
		{
			completeNote(output, &velocities, startMessage, jmax(lastEndTimestamp - startMessage->getTimeStamp(), minNoteLength));
		}
	}
}
