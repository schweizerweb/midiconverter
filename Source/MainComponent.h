/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SettingsComponent.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   : public Component, Button::Listener
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&) override;
    void resized() override;

private:
	bool checkSettingsValid() const;
	bool startProcess() const;
	void buttonClicked(Button* btn) override;

	ScopedPointer<SettingsComponent> settingsComponent;
	ScopedPointer<TextButton> startButton;
	ScopedPointer<MidiSettings> settings;
	ApplicationProperties properties;
	PropertiesFile* userSettings;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};
