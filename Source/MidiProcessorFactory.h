/*
  ==============================================================================

    MidiProcessorFactory.h
    Created: 25 Feb 2018 11:26:25pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#pragma once
#include "MidiProcessor.h"
#include "MidiSettings.h"

class MidiProcessorFactory
{
public:
	static MidiProcessor* getProcessor(MidiSettings::eAlgorithm algorithm);
};
