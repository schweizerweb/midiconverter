/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.3.0

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
#include "../JuceLibraryCode/JuceHeader.h"
#include "MidiSettings.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class SettingsComponent  : public Component,
                           public TextEditor::Listener,
                           public Button::Listener,
                           public Slider::Listener,
                           public ComboBox::Listener
{
public:
    //==============================================================================
    SettingsComponent (MidiSettings* pSettings);
    ~SettingsComponent();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
	void textEditorTextChanged(TextEditor& textEditor) override;
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void buttonClicked (Button* buttonThatWasClicked) override;
    void sliderValueChanged (Slider* sliderThatWasMoved) override;
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.
	MidiSettings* pSettings;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<TextEditor> textMidiFile;
    ScopedPointer<Label> labelMidiFile;
    ScopedPointer<TextButton> btnChooseFile;
    ScopedPointer<Label> labelMinInterval;
    ScopedPointer<Slider> sliderMinInterval;
    ScopedPointer<Label> labelToneLength;
    ScopedPointer<Slider> sliderToneLength;
    ScopedPointer<TextEditor> textOutputName;
    ScopedPointer<Label> labelOutput;
    ScopedPointer<Label> labelAlgorithm;
    ScopedPointer<ComboBox> comboBoxAlgorithm;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SettingsComponent)
};

//[EndFile] You can add extra defines here...
//[/EndFile]
