/*
  ==============================================================================

    MidiProcessorAblingerNew.h
    Created: 25 Feb 2018 11:25:44pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#pragma once
#include "MidiProcessor.h"

class MidiProcessorAblingerNew : public MidiProcessor
{
public:
	MidiProcessorAblingerNew(bool versionB);
	~MidiProcessorAblingerNew() override;
	void process(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double* tickLengthMs) override;

private:
	static int getMaxVelocityIndex(OwnedArray<MidiMessage>* array);
	bool versionB;
};
