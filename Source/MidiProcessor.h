/*
  ==============================================================================

    MidiProcessor.h
    Created: 25 Feb 2018 11:26:04pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#pragma once
#include "JuceHeader.h"
#include "MidiSettings.h"
#define NB_MIDI_NOTES 128

class MidiProcessor
{
public:
	virtual ~MidiProcessor() = 0;
	virtual void process(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double* tickLengthMs) = 0;
	
protected:
	double getTicks(int ms, short timeFormat, double tickLengthMs);
	void completeNote(MidiMessageSequence* pTrack, Array<float>* velocities, MidiMessage* startMessage, double noteLength) const;
	void prepareNoteArrays(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double* tickLengthMs, bool generatePseudoHighResolution);
	
protected:
	double minNoteLength;
	double minInterval;
	double minBreakLength;
	OwnedArray<MidiMessage> noteSortedEvents[NB_MIDI_NOTES];
};
