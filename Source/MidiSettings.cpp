/*
  ==============================================================================

    MidiSettings.cpp
    Created: 1 Feb 2018 8:54:13am
    Author:  Christian Schweizer

  ==============================================================================
*/

#include "MidiSettings.h"

MidiSettings::MidiSettings()
{
	file = nullptr;
	minIntervalMs = 100;
	noteLengthMs = 72;
	algorithm = Simple;
}
