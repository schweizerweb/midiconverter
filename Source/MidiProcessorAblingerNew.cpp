/*
  ==============================================================================

    MidiProcessorAblingerNew.cpp
    Created: 25 Feb 2018 11:25:44pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#include "MidiProcessorAblingerNew.h"
#include "MidiMessageComparator.h"

MidiProcessorAblingerNew::MidiProcessorAblingerNew(bool versionB) : versionB(versionB)
{
}

MidiProcessorAblingerNew::~MidiProcessorAblingerNew()
{
}

void MidiProcessorAblingerNew::process(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double* tickLengthMs)
{
	prepareNoteArrays(input, output, settings, tickLengthMs, false);

	for (int iNote = 0; iNote < 128; iNote++)
	{
		Array<MidiMessage> resultEvents;

		if (noteSortedEvents[iNote].size() < 1)
			continue;

		int index;
		while((index = getMaxVelocityIndex(&noteSortedEvents[iNote])) >= 0)
		{
			MidiMessage* currentMessage = noteSortedEvents[iNote].getUnchecked(index);
			double startTs = currentMessage->getTimeStamp();
			double stopTs = startTs + minInterval;

			// check existing notes
			bool ignore = false;
			for (MidiMessage mm : resultEvents)
			{
				if (mm.getTimeStamp() >= startTs && mm.getTimeStamp() <= stopTs)
				{
					if (versionB)
					{
						// TODO
						AlertWindow::showMessageBox(AlertWindow::WarningIcon, ProjectInfo::projectName, "Not implemented yet!");
						return;
					}
					else
					{
						ignore = true;
						break;
					}
				}
			}

			if (ignore)
			{
				currentMessage->setVelocity(0.0);
			}
			else 
			{
				// add note to output
				resultEvents.add(*currentMessage);

				// set velocity of current note and all following notes to 0 in input
				for (int i = 0; i < noteSortedEvents[iNote].size(); i++)
				{
					double ts = noteSortedEvents[iNote].getUnchecked(i)->getTimeStamp();
					if (ts >= startTs && ts <= stopTs)
						noteSortedEvents[iNote].getUnchecked(i)->setVelocity(0.0);
				}
			}
		}

		// write to output
		MidiMessageComparator comparator;
		resultEvents.sort(comparator);
		for (int i = 0; i < resultEvents.size(); i++)
		{
			MidiMessage mm = resultEvents.getReference(i);
			output->addEvent(mm);

			double noteLength = minInterval;

			if (i < resultEvents.size() - 1)
			{
				MidiMessage followingMm = resultEvents.getReference(i + 1);
				noteLength = jmin(noteLength, followingMm.getTimeStamp() - mm.getTimeStamp() - minBreakLength);
			}

			MidiMessage off = MidiMessage::noteOff(mm.getChannel(), mm.getNoteNumber());
			off.setTimeStamp(mm.getTimeStamp() + noteLength);
			output->addEvent(off);
		}
	}
}

int MidiProcessorAblingerNew::getMaxVelocityIndex(OwnedArray<MidiMessage>* array)
{
	int maxVelocity = 0;
	int index = -1;

	for(int i = 0; i < array->size(); i++)
	{
		if(array->getUnchecked(i)->getVelocity() > maxVelocity)
		{
			maxVelocity = array->getUnchecked(i)->getVelocity();
			index = i;
		}
	}

	return index;
}
