/*
  ==============================================================================

    MidiProcessorSimple.h
    Created: 25 Feb 2018 11:25:30pm
    Author:  Christian Schweizer

  ==============================================================================
*/

#pragma once
#include "MidiProcessor.h"

class MidiProcessorSimple : public MidiProcessor
{
public:
	~MidiProcessorSimple() override;
	void process(const MidiMessageSequence* input, MidiMessageSequence* output, MidiSettings* settings, double* tickLengthMs) override;
};
